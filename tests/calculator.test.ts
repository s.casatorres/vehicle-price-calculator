import fs from 'fs';
import request from 'supertest';

import { app } from '../src/app';
import { DEFAULT_VALUES } from './../src/config/config';
import { CALCULATOR_RESPONSES } from './fixtures/calculator';
import { CREDIT_PROMOTIONS, PERCENTAGE_PROMOTIONS, PROMOCODES_LIST } from './fixtures/promocodes';
import { VEHICLES, VEHICLES_LIST } from './fixtures/vehicles';

beforeAll(async () => {
  const vehiclesFilePath =
    process.env.VEHICLES_FILE_PATH && process.env.VEHICLES_FILE_NAME
      ? `${process.env.VEHICLES_FILE_PATH}/${process.env.VEHICLES_FILE_NAME}`
      : `${DEFAULT_VALUES.FILE_PATH}/${DEFAULT_VALUES.VEHICLES_FILE_NAME}`;

  const promocodesFilePath =
    process.env.PROMOCODES_FILE_PATH && process.env.PROMOCODES_FILE_PATH
      ? `${process.env.PROMOCODES_FILE_PATH}/${process.env.PROMOCODES_FILE_PATH}`
      : `${DEFAULT_VALUES.FILE_PATH}/${DEFAULT_VALUES.PROMOCODES_FILE_NAME}`;

  if (!fs.existsSync(vehiclesFilePath)) {
    await request(app).post('/vehicles').send(VEHICLES_LIST).expect(202);
  }

  if (!fs.existsSync(promocodesFilePath)) {
    await request(app).post('/promocodes').send(PROMOCODES_LIST).expect(202);
  }
});

describe('Calculator endpoint', () => {
  it('should return 404 if "id" path param is missing', async () => {
    await request(app).get('/calculator').expect(404);
  });

  it('should return 400 if "code" query param is missing', async () => {
    await request(app).get('/calculator/1').expect(400);
  });

  it('should return 400 if "code" query param is empty', async () => {
    await request(app).get('/calculator/1?code=').expect(400);
  });

  it('should return 404 if vehicle does not exists', async () => {
    const promocode = PERCENTAGE_PROMOTIONS.allVehicles.code;

    await request(app).get(`/calculator/1?code=${promocode}`).expect(404);
  });

  it('should return 400 if "code" is not applicable to provided vehicle', async () => {
    const vehicleId = VEHICLES.notOnOffer._id;
    const promocode = CREDIT_PROMOTIONS.someVehicles.code;

    await request(app).get(`/calculator/${vehicleId}?code=${promocode}`).expect(400);
  });

  it('should return correct price for an all vehicles percentage promotion and a "not on offer vehicle"', async () => {
    const vehicleId = VEHICLES.notOnOffer._id;
    const promocode = PERCENTAGE_PROMOTIONS.allVehicles.code;
    const mockedResponse = CALCULATOR_RESPONSES.notOnOfferVehicle.allVehiclesPercentagePromotion;

    const response = await request(app).get(`/calculator/${vehicleId}?code=${promocode}`);

    expect(response.body).toStrictEqual(mockedResponse);
    expect(response.status).toBe(200);
  });

  it('should return correct price for an all vehicles percentage promotion and an "on offer vehicle"', async () => {
    const vehicleId = VEHICLES.onOffer._id;
    const promocode = PERCENTAGE_PROMOTIONS.allVehicles.code;
    const mockedResponse = CALCULATOR_RESPONSES.onOfferVehicle.allVehiclesPercentagePromotion;

    const response = await request(app).get(`/calculator/${vehicleId}?code=${promocode}`);

    expect(response.body).toStrictEqual(mockedResponse);
    expect(response.status).toBe(200);
  });

  it('should return correct price for a credit promotion and a "not on offer vehicle"', async () => {
    const vehicleId = VEHICLES.notOnOffer._id;
    const promocode = CREDIT_PROMOTIONS.allVehicles.code;
    const mockedResponse = CALCULATOR_RESPONSES.notOnOfferVehicle.allVehiclesCreditPromotion;

    const response = await request(app).get(`/calculator/${vehicleId}?code=${promocode}`);

    expect(response.body).toStrictEqual(mockedResponse);
    expect(response.status).toBe(200);
  });

  it('should return correct price for a credit promotion and an "on offer vehicle"', async () => {
    const vehicleId = VEHICLES.onOffer._id;
    const promocode = CREDIT_PROMOTIONS.allVehicles.code;
    const mockedResponse = CALCULATOR_RESPONSES.onOfferVehicle.allVehiclesCreditPromotion;

    const response = await request(app).get(`/calculator/${vehicleId}?code=${promocode}`);

    expect(response.body).toStrictEqual(mockedResponse);
    expect(response.status).toBe(200);
  });
});
