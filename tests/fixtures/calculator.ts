export const CALCULATOR_RESPONSES = {
  notOnOfferVehicle: {
    allVehiclesPercentagePromotion: {
      price: 359.99,
      discountByOffer: 0,
      discountByCode: 36,
    },
    allVehiclesCreditPromotion: {
      price: 359.99,
      discountByOffer: 0,
      discountByCode: 25,
    },
  },
  onOfferVehicle: {
    allVehiclesPercentagePromotion: {
      price: 559.99,
      discountByOffer: 60,
      discountByCode: 50,
    },
    allVehiclesCreditPromotion: {
      price: 559.99,
      discountByOffer: 60,
      discountByCode: 25,
    },
  },
};
