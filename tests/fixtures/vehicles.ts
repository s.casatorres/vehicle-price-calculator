export const VEHICLES = {
  notOnOffer: {
    _id: 'vehicle1',
    price: 359.99,
    priceOnOffer: 299.99,
    isOnOffer: false,
  },
  onOffer: {
    _id: 'vehicle2',
    price: 559.99,
    priceOnOffer: 499.99,
    isOnOffer: true,
  },
};

export const VEHICLES_LIST = Object.keys(VEHICLES).reduce((vehicles, key) => {
  vehicles.push(VEHICLES[key]);
  return vehicles;
}, []);
