export const PERCENTAGE_PROMOTIONS = {
  someVehicles: {
    _id: 'promocode1',
    code: 'FIAT50',
    discount: 50,
    type: 'PERCENTAGE',
    vehicles: ['vehicle1'],
  },
  allVehicles: {
    _id: 'promocode2',
    code: 'ALL10',
    discount: 10,
    type: 'PERCENTAGE',
    vehicles: [],
  },
};

export const CREDIT_PROMOTIONS = {
  someVehicles: {
    _id: 'promocode3',
    code: 'MERCEDES100',
    discount: 100,
    type: 'CREDIT',
    vehicles: ['vehicle2'],
  },
  allVehicles: {
    _id: 'promocode4',
    code: 'ALL25',
    discount: 25,
    type: 'CREDIT',
    vehicles: [],
  },
};

export const INVALID_PROMOTION = {
  _id: 'invalidPromo',
  code: 'INVALID',
  discount: 100,
  type: '',
  vehicles: [],
};

export const PROMOCODES_LIST = [
  ...Object.keys(PERCENTAGE_PROMOTIONS).reduce((promocodes, key) => {
    promocodes.push(PERCENTAGE_PROMOTIONS[key]);
    return promocodes;
  }, []),
  ...Object.keys(CREDIT_PROMOTIONS).reduce((promocodes, key) => {
    promocodes.push(CREDIT_PROMOTIONS[key]);
    return promocodes;
  }, []),
];
