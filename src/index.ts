import { app } from './app';
import { DEFAULT_VALUES } from './config/config';

const port = process.env.PORT || DEFAULT_VALUES.PORT;

app.listen(port, () => {
  // tslint:disable-next-line:no-console
  console.log(`Server is up and running on port ${port}`);
});
