import express, { Request, Response } from 'express';
import { Container } from 'typedi';

import { validate } from '../middleware/validator';
import { calculatorValidationRules } from '../middleware/validators/calculator';
import CalculatorService from '../services/calculator';
import PromocodesService from '../services/promocodes';
import VehiclesService from '../services/vehicles';

const router = express.Router();
const calculatorService = Container.get<CalculatorService>(CalculatorService);
const vehiclesService = Container.get<VehiclesService>(VehiclesService);
const promocodesService = Container.get<PromocodesService>(PromocodesService);

router.get('/:id', calculatorValidationRules(), validate, async (req: Request, res: Response) => {
  try {
    const { id: vehicleId } = req.params;
    const { code: promocodeId } = req.query;

    const vehicle = await vehiclesService.getVehicle(vehicleId);
    if (!vehicle) {
      return res.status(404).send('Vehicle doest not exists.');
    }

    const promocode = await promocodesService.getPromocode(promocodeId as string);
    const isApplicable = promocodesService.isPromocodeApplicable(promocode, vehicle._id);
    if (!isApplicable) {
      return res.status(400).send('Promocode is not applicable to this car.');
    }

    const finalPrice = calculatorService.calculatePrice(promocode, vehicle);

    return res.status(200).send(finalPrice);
  } catch (err) {
    return res.status(500).send(err);
  }
});

export { router as CalculatorRouter };
