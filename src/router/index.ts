import express from 'express';

import { CalculatorRouter } from './calculator';
import { PromocodesRouter } from './promocodes';
import { VehiclesRouter } from './vehicles';

const router = express.Router();

router.use('/vehicles', VehiclesRouter);
router.use('/promocodes', PromocodesRouter);
router.use('/calculator', CalculatorRouter);

export { router as MainRouter };
