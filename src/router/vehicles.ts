import express, { Request, Response } from 'express';
import { Container } from 'typedi';

import { validate } from '../middleware/validator';
import { vehicleValidationRules } from '../middleware/validators/vehicles';
import VehiclesService from '../services/vehicles';
import { Vehicle } from './../models/vehicle';

const router = express.Router();
const vehiclesService = Container.get<VehiclesService>(VehiclesService);

router.post('/', vehicleValidationRules(), validate, async (req: Request, res: Response) => {
  try {
    const { body: vehicles } = req;

    const currentVehicles: Vehicle[] = await vehiclesService.getVehicles();
    await vehiclesService.addVehicles([...currentVehicles, ...vehicles]);

    return res.status(202).send();
  } catch (err) {
    return res.status(500).send(err);
  }
});

export { router as VehiclesRouter };
