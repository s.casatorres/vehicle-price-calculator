import express, { Request, Response } from 'express';
import { Container } from 'typedi';

import { validate } from '../middleware/validator';
import { promocodesValidationRules } from '../middleware/validators/promocodes';
import PromocodesService from '../services/promocodes';
import { Promocode } from './../models/promocode';

const router = express.Router();
const promocodesService = Container.get<PromocodesService>(PromocodesService);

router.post('/', promocodesValidationRules(), validate, async (req: Request, res: Response) => {
  try {
    const { body: promocodes } = req;

    const currentPromocodes: Promocode[] = await promocodesService.getPromocodes();
    await promocodesService.addPromocodes([...currentPromocodes, ...promocodes]);

    return res.status(202).send();
  } catch (err) {
    return res.status(500).send(err);
  }
});

export { router as PromocodesRouter };
