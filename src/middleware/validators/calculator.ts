import { query } from 'express-validator';

const validationRules = () => {
  return [query('code', 'Required query param.').exists(), query('code', 'Must be a non-empty string.').notEmpty()];
};

export { validationRules as calculatorValidationRules };
