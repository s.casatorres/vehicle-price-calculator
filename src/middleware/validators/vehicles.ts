import { body } from 'express-validator';

const validationRules = () => {
  return [
    body().isArray(),
    body(['*._id', '*.price', '*.priceOnOffer', '*.isOnOffer'], 'Required field.').exists(),
    body('*._id', 'Must be a non-empty string').isString().notEmpty(),
    body(['*.price', '*.priceOnOffer'], 'Must be a number.').custom((value) => typeof value === 'number'),
    body('*.isOnOffer', 'Must be a boolean.').custom((value) => typeof value === 'boolean'),
  ];
};

export { validationRules as vehicleValidationRules };
