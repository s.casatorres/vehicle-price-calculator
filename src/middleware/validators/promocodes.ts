import { body } from 'express-validator';

import { promoTypesList } from '../../models/promocode';

const validationRules = () => {
  return [
    body().isArray(),
    body(['*._id', '*.code', '*.discount', '*.type', '*.vehicles'], 'Required field.').exists(),
    body(['*._id', '*.code', '*.type'], 'Must be a non-empty string').isString().notEmpty(),
    body('*.discount', 'Must be a number.').custom((value) => typeof value === 'number'),
    body('*.type', 'Invalid type.').isIn(promoTypesList),
    body('*.vehicles', 'Must be an array.').isArray(),
    body('*.vehicles.*', 'Must be a non-empty string').isString().notEmpty(),
  ];
};

export { validationRules as promocodesValidationRules };
