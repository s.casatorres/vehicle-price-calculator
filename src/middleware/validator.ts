import { NextFunction, Request, Response } from 'express';
import { validationResult } from 'express-validator';

const validate = (req: Request, res: Response, next: NextFunction) => {
  const errors = validationResult(req);
  if (errors.isEmpty()) {
    return next();
  }

  const mappedErrors = errors.array().reduce((err, currentError) => {
    const param = currentError.param || currentError.location;

    err[param] = err[param] ? [...err[param], currentError.msg] : [currentError.msg];

    return err;
  }, {});

  return res.status(400).json({
    errors: mappedErrors,
  });
};

export { validate };
