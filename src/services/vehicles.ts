import { DEFAULT_VALUES } from '../config/config';
import { Vehicle } from './../models/vehicle';
import EntitiesService from './entities';

export default class VehiclesService extends EntitiesService<Vehicle> {
  public async addVehicles(vehicles: Vehicle[]): Promise<void> {
    return this.addEntities(vehicles);
  }

  public async getVehicles(): Promise<Vehicle[]> {
    await this.loadVehiclesFilePath();

    return this.getEntities();
  }

  public async getVehicle(id: string): Promise<Vehicle> {
    const vehicles: Vehicle[] = await this.getVehicles();

    return vehicles.find((vehicle) => vehicle._id === id);
  }

  private async loadVehiclesFilePath(): Promise<void> {
    return this.loadFilePath();
  }

  protected getFilePath(): string {
    return process.env.VEHICLES_FILE_PATH ? process.env.VEHICLES_FILE_PATH : DEFAULT_VALUES.FILE_PATH;
  }

  protected getFileName(): string {
    return process.env.VEHICLES_FILE_NAME ? process.env.VEHICLES_FILE_NAME : DEFAULT_VALUES.VEHICLES_FILE_NAME;
  }

  protected getFullPath(): string {
    return [this.getFilePath(), this.getFileName()].join('/');
  }
}
