import { DEFAULT_VALUES } from './../config/config';
import { Promocode } from './../models/promocode';
import EntitiesService from './entities';

export default class PromocodesService extends EntitiesService<Promocode> {
  public async addPromocodes(promocodes: Promocode[]): Promise<void> {
    return this.addEntities(promocodes);
  }

  public async getPromocodes(): Promise<Promocode[]> {
    await this.loadPromocodesFilePath();

    return this.getEntities();
  }

  public async getPromocode(code: string): Promise<Promocode> {
    const promocodes: Promocode[] = await this.getPromocodes();

    return promocodes.find((promocode) => promocode.code === code);
  }

  public async loadPromocodesFilePath(): Promise<void> {
    return this.loadFilePath();
  }

  public isPromocodeApplicable(promocode: Promocode, vehicleId: string): boolean {
    return promocode && (!promocode.vehicles.length || promocode.vehicles.some((id) => id === vehicleId));
  }

  protected getFilePath(): string {
    return process.env.PROMOCODES_FILE_PATH ? process.env.PROMOCODES_FILE_PATH : DEFAULT_VALUES.FILE_PATH;
  }

  protected getFileName(): string {
    return process.env.PROMOCODES_FILE_NAME ? process.env.PROMOCODES_FILE_NAME : DEFAULT_VALUES.PROMOCODES_FILE_NAME;
  }

  protected getFullPath(): string {
    return [this.getFilePath(), this.getFileName()].join('/');
  }
}
