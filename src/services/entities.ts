import fs from 'fs';

export default abstract class EntitiesService<T> {
  protected async addEntities(entities: T[]): Promise<void> {
    return new Promise((resolve, reject) => {
      try {
        const entitiesJSON = JSON.stringify(entities);

        fs.writeFile(this.getFullPath(), entitiesJSON, (err) => {
          if (err) {
            throw new Error(err.message);
          }

          resolve();
        });
      } catch (err) {
        reject(err);
      }
    });
  }

  protected async getEntities(): Promise<T[]> {
    return new Promise((resolve, reject) => {
      try {
        fs.readFile(this.getFullPath(), { flag: 'a+' }, (readFileErr, data) => {
          if (readFileErr) {
            throw new Error(readFileErr.message);
          }

          const entitiesJSON = data.toString();
          const entities: T[] = entitiesJSON ? JSON.parse(entitiesJSON) : [];

          return resolve(entities);
        });
      } catch (err) {
        return reject(err);
      }
    });
  }

  protected async loadFilePath(): Promise<void> {
    return new Promise((resolve, reject) => {
      try {
        fs.mkdir(this.getFilePath(), { recursive: true }, (mkdirErr) => {
          if (mkdirErr) {
            throw new Error(mkdirErr.message);
          }

          resolve();
        });
      } catch (err) {
        reject(err);
      }
    });
  }

  protected abstract getFilePath(): string;

  protected abstract getFileName(): string;

  protected abstract getFullPath(): string;
}
