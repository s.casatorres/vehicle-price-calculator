import Container from 'typedi';

import { CalculatedPrice } from './../models/calculator';
import { PROMO_TYPES, Promocode } from './../models/promocode';
import { Vehicle } from './../models/vehicle';
import UtilsService from './utils';

const utilsService = Container.get<UtilsService>(UtilsService);

export default class CalculatorService {
  public calculatePrice(promocode: Promocode, vehicle: Vehicle): CalculatedPrice {
    const vehiclePrice = vehicle.isOnOffer ? vehicle.priceOnOffer : vehicle.price;

    const discountByOffer = this.getDiscountByOffer(vehicle);
    const discountByCode = this.getDiscountByCode(promocode, vehiclePrice);

    const roundedDiscountByOffer = utilsService.round(discountByOffer, 2);
    const roundedDiscountByCode = utilsService.round(discountByCode, 2);

    return {
      price: vehicle.price,
      discountByOffer: roundedDiscountByOffer,
      discountByCode: roundedDiscountByCode,
    };
  }

  private getDiscountByOffer(vehicle: Vehicle): number {
    return vehicle.isOnOffer ? vehicle.price - vehicle.priceOnOffer : 0;
  }

  private getDiscountByCode(promocode: Promocode, vehiclePrice: number): number {
    switch (promocode.type) {
      case PROMO_TYPES.CREDIT: {
        return promocode.discount;
      }

      case PROMO_TYPES.PERCENTAGE: {
        return this.applyPercentageDiscount(promocode.discount, vehiclePrice);
      }

      default: {
        return 0;
      }
    }
  }

  private applyPercentageDiscount(discount: number, price: number): number {
    return (price * Math.abs(discount)) / 100;
  }
}
