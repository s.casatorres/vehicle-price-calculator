export default class UtilsService {
  public round(value: number, decimals: number): number {
    const decimalPlaces = Math.abs(decimals);
    // tslint:disable-next-line: prefer-template
    return Number(Math.round(((value + 'e' + decimalPlaces) as unknown) as number) + 'e-' + decimalPlaces);
  }
}
