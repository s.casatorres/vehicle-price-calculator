import express, { Request, Response } from 'express';

import { MainRouter } from './router';

const app = express();

app.use(express.json());

app.use('/', MainRouter);
app.use((req: Request, res: Response) => {
  return res.status(404).send('404: Resource not found');
});

export { app };
