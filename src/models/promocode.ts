export type PromoType = 'CREDIT' | 'PERCENTAGE';

export enum PROMO_TYPES {
  CREDIT = 'CREDIT',
  PERCENTAGE = 'PERCENTAGE',
}

export const promoTypesList = Object.keys(PROMO_TYPES).map((key) => PROMO_TYPES[key]);

export interface Promocode {
  _id: string;
  code: string;
  discount: number;
  type: PromoType;
  vehicles: string[];
}
