export interface Vehicle {
  _id: string;
  price: number;
  priceOnOffer: number;
  isOnOffer: boolean;
}
