export interface CalculatedPrice {
  price: number;
  discountByOffer: number;
  discountByCode: number;
}
