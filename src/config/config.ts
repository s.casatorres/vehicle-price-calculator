export const DEFAULT_VALUES = {
  PORT: 3000,
  FILE_PATH: './data',
  VEHICLES_FILE_NAME: 'vehicles.json',
  PROMOCODES_FILE_NAME: 'promocodes.json',
};
