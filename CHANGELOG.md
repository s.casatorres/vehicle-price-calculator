## [1.1.0] - 2020-11-11

### Removed

- Useless links in README.md

## [1.0.0] - 2020-11-11

Released stabe version!

### Added

- Default constants files.
- Calculator missing model.
- CHANGELOG.md.

### Changed

- README.md to add a project summary.

## [0.6.0] - 2020-11-10

### Added

- Calculator endpoint tests.

## [0.5.0] - 2020-11-10

### Added

- Calculator endpoint and structure.

### Changed

- Validators are now on their own folder.

## [0.4.0] - 2020-11-06

### Added

- Entities service abstract class.

### Changed

- Vehicles and promocodes services now extend entities service.

## [0.3.0] - 2020-11-06

### Added

- Promocodes endpoint (router, service, models...) and fields validator.

## [0.2.0] - 2020-11-06

### Added

- Vehicles endpoint (router, service, models...) and fields validator
- Generic field validator middleware.

### Removed

- Whole initial structure project endpoint (greeting).

## [0.1.0] - 2020-11-05

### Added

- Initial nodejs + express (with ts) structure.
- Jest as testing framework.
- Gitlab CI/CD config + Heroku deployment.
