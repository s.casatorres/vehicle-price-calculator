<h1 align="center">Welcome to vehicle-price-calculator 👋</h1>
<p>
  <img alt="Version" src="https://img.shields.io/badge/version-1.1.0-blue.svg?cacheSeconds=2592000" />
  <img alt="Lines coverage" src="https://img.shields.io/gitlab/coverage/s.casatorres/vehicle-price-calculator/master" />
  <img alt="Build" src="https://img.shields.io/gitlab/pipeline/s.casatorres/vehicle-price-calculator/master" />
  <img alt="License: ISC" src="https://img.shields.io/badge/License-ISC-yellow.svg" />
</p>

> A simple vehicle price calculator based on vehicles and promocodes.

## Environments

### Develop

```sh
https://vehicle-price-calculator-dev.herokuapp.com/
```

### Production

```sh
https://vehicle-price-calculator.herokuapp.com/
```

## Install

```sh
npm install
```

## Usage

### Start project from dist build

```sh
npm run start
```

### Build

```sh
npm run build
```

### Start a dev server with nodemon

```sh
npm run dev
```

### Run tests

```sh
npm run test
```

### Run tslint

```sh
npm run tslint
```

## Author

👤 **Sergio Casatorres Hernández**

- Gitlab: [@s.casatorres](https://gitlab.com/s.casatorres)
- LinkedIn: [@scasatorres](https://linkedin.com/in/scasatorres)

## 🤝 Contributing

Contributions, issues and feature requests are welcome!<br />Feel free to check [issues page](https://gitlab.com/s.casatorres/vehicle-price-calculator/issues).

## Show your support

Give a ⭐️ if this project helped you!

---

_This README was generated with ❤️ by [readme-md-generator](https://github.com/kefranabg/readme-md-generator)_
